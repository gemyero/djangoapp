# Prerequisites
1. Python3
2. Django 2.0.4

# To start the app
1. clone the repo
2. cd djangoApp
3. python3 manage.py makemigrations
4. python3 manage.py migrate
5. python3 manage.py runserver
6. Open your browser and type http://localhost:8000