from django.shortcuts import render, redirect
from .models import User
from .forms import UserForm


def list_users(request):
    users = User.objects.all()
    context = {'users': users}
    return render(request, 'user/list.html', context)


def add_user(request):

    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            user = User(**form.cleaned_data)
            user.save()
            return redirect('list')
    else:
        form = UserForm()

    return render(request, 'user/add.html', {'form': form})


def welcome(request):
    return render(request, 'user/welcome.html')
